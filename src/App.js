import './App.css';
import React, { useEffect, useState } from 'react';
import Audition from './components/audition';
import Header from './components/header';
import {makeStyles} from '@material-ui/core/styles';
import AuditionService from './services/AuditionService';
import {Grid} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  body: {
    margin: '10px'
  },
  title: {
    textAlign: 'center',
    margin: '20px'
  }
}))

function App() {
  const [auditions, setAuditions] = useState(null);
  const [title, setTitle] = useState('');
  const [company, setCompany] = useState('');
  const [description, setDescription] = useState('');
  const [roles, setRoles] = useState('');
  const [gender, setGender] = useState('');
  const [isPaid, setIsPaid] = useState(false);
  const [isUnion, setIsUnion] = useState(false);
  const [requirements, setRequirements] = useState('');
  const [contactEmail, setContactEmail] = useState('');

  useEffect(() => {
    let mounted = true;
    AuditionService.getAuditions()
    .then(auditions => {
        if(mounted){
            setAuditions(auditions.data)
        }
    })
    return () => mounted = false;
}, [])


  const saveAudition = (event) => {
    
    let audition = {
      title: `${title}`, 
      company:`${company}`, 
      description: `${description}`,
      roles: `${roles}`,
      gender: `${gender}`,
      isPaid: `${isPaid}`,
      isUnion: `${isUnion}`,
      requirements: `${requirements}`,
      contactEmail: `${contactEmail}`
    };

    console.log('audition: ' + JSON.stringify(audition));

    AuditionService.addAudition(audition);
  }

  const changeTitle = (event) => {
    setTitle(event.target.value);
  }

  const changeCompany = (event) => {
    setCompany(event.target.value);
  }

  const changeDescription = (event) => {
    setDescription(event.target.value);
  }

  const changeRoles = (event) => {
    setRoles(event.target.value);
  }

  const changeGender = (event) => {
    setGender(event.target.value);
  }

  const changePaid = (event) => {
    setIsPaid(event.target.value);
  }

  const changeUnion = (event) => {
    setIsUnion(event.target.value);
  }
  const changeRequirements = (event) => {
    setRequirements(event.target.value);
  }
  const changeEmail = (event) => {
    setContactEmail(event.target.value);
  }

  const classes = useStyles();

  return (
    <div className={classes.body}>
      <Header/>
      <hr/>
      <h2 className={classes.title}>My Auditions</h2>
      <Grid container spacing={6}>
        <Grid item alignItems='left' justify='left' direction='column' xs={6}>
      <table className="table table-striped">
        <tbody>
          {auditions ? auditions.map(audition => {
            return <Audition  data={audition} />
            }) : 'loading data...'}
        </tbody>
      </table>
      </Grid>
        <Grid item justify="right" direction="column" alignItems='right' xs={6}>
      <form>
        <h3>Create New</h3>
        <label>
          Title: 
          <input name="title" value={title} onChange={changeTitle}/>
        </label>
        <br/>
        <label>
          Company: 
          <input type="text" name="company" value={company} onChange={changeCompany}/>
        </label>
        <br/>
        <label>
          Description: 
          <input type="text" name="description" value={description} onChange={changeDescription}/>
        </label>
        <br/>
        <label>
          Roles Available: 
          <input type="text" name="roles" value={roles} onChange={changeRoles}/>
        </label>
        <br/>
        <label>
          Gender: 
          <input type="text" name="gender" value={gender} onChange={changeGender}/>
        </label>
        <br/>
        <label>
          Is this job paid?: 
          <input type="text" name="isPaid" value={isPaid} onChange={changePaid}/>
        </label>
        <br/>
        <label>
          Union Status: 
          <input type="text" name="isUnion" value={isUnion} onChange={changeUnion}/>
        </label>
        <br/>
        <label>
          What to Prepare: 
          <input type="text" name="requirements" value={requirements} onChange={changeRequirements}/>
        </label>
        <br/>
        <label>
          Contact Email: 
          <input type="text" name="contactEmail" value={contactEmail} onChange={changeEmail}/>
        </label>
        <br/>
        <button className="btn btn-success" onClick={saveAudition}>Save</button>
      </form>
      </Grid>
      </Grid>
    </div>
  );
}

export default App;
