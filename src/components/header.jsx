import React, { useEffect, useState } from 'react';
import {AppBar, Toolbar, Typography, Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

const Header = () => {

    const useStyles = makeStyles(() => ({
        nav: {
            backgroundColor: "maroon"
        }
      }))

    const classes = useStyles();

    return (
        <>
            <AppBar position="static" className={classes.nav}>
                <Toolbar>
                    <Typography variant="h4">
                    <i class="fas fa-theater-masks"></i>
                    ShowBook
                    </Typography>
                </Toolbar>
            </AppBar>
        </>
    );              
};


export default Header;