import React, { useEffect, useState } from 'react';
import AuditionService from '../services/AuditionService';

const Audition = (props) => {

    const [audition, setAudition] = useState(props.data);
    
    return (
        <>
            <tr>
                <td><h3>{audition.title}</h3>
                <p>{audition.company}</p>
                <p>{audition.description}</p>
                <p>Roles Available: {audition.roles}</p>
                <p>{audition.gender}</p>
                <p>{audition.isPaid ? "PAID" : "UNPAID"}</p>
                <p>{audition.isUnion ? "UNION" : "NON-UNION"}</p>
                <p>What to Prepare: {audition.requirements}</p>
                <p>Contact: {audition.contactEmail}</p>
                </td>
            </tr>
        </>
    );              
};


export default Audition;