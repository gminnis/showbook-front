import axios from 'axios'
import react from 'react'

const AUDITION_REST_API_URL = 'http://localhost:8081/auditions'

class AuditionService {

    getAuditions(){
         return axios (AUDITION_REST_API_URL)        
    }

    addAudition(audition) {
        return axios.post('http://localhost:8081/auditions/create', audition)
    }

}

export default new AuditionService()
